<?php
defined('BASEPATH') or exit('No direct script access allowed');

class SettingProfile extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model('AuthModel');
    }

    public function index()
    {
        $data['title'] = "Settings Profile";
        $data['getDataProfileUser'] = $this->AuthModel->getUserProfile();
        $data['content'] = "dashboards/settings_profile";
        $this->load->view('dashboards/client/index_client', $data);
    }

    public function updateProfile($id)
    {
        if ($this->input->post('submit')) {
            if (!empty($_FILES['uploadPhoto']['name'])) {
                $config['upload_path'] = './assets/img/client';
                $config['allowed_types'] = 'jpg|png';
                $config['max_size'] = '1048';
                $config['file_name'] = base64_encode($_FILES['uploadPhoto']['name']);

                $this->load->library('upload', $config);

                if (!$this->upload->do_upload('uploadPhoto')) {
                    $error = array('error' => $this->upload->display_errors());
                    $this->session->set_flashdata('notif', '<div class="alert alert-danger" role="alert"><strong>Update Profile Failed !!</strong>
                        "' . $error['error'] . '"
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                      <span aria-hidden="true">&times;</span></button></div>');
                } else {
                    $data = $this->upload->data();
                    if ($this->AuthModel->updateProfile($id, $data['file_name']) == TRUE) {
                        $this->session->set_flashdata('notif', '<div class="alert alert-success" role="alert"><strong>Update Profile Success !!</strong><button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span></button></div>');
                    } else {
                        echo "gagal update";
                    }
                }
                redirect('SettingProfile');
            } else {
                if ($this->AuthModel->updateProfile($id, "") == TRUE) {
                    $this->session->set_flashdata('notif', '<div class="alert alert-success" role="alert"><strong>Update Profile Success !!</strong><button type="button" class="close" data-dismiss="alert" aria-label="Close">
                      <span aria-hidden="true">&times;</span></button></div>');
                } else {
                    echo "gagal update";
                }
                redirect('SettingProfile');
            }
        }
    }
}

/* End of file Controllername.php */

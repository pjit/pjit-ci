
<div clss="container-fluid">
<?= $this->session->flashdata('notif') ?></div>
<div class="settings-profile-place">
	<div class="form-edit-profile-place">
	<?php foreach ($getDataProfileUser as $key => $getDataProfileUsers) : ?>
		<form action="<?= base_url('SettingProfile/updateProfile/'.$getDataProfileUsers->id_user) ?>" method="post" enctype="multipart/form-data">
			<span class="title main-color">
				Edit Profile
			</span>
			<div class="photo-profile-place">
				<img src="<?= base_url()?>assets/img/client/<?=$getDataProfileUsers->picture?>" class="photo-profile">
				<!-- <input type="button" name="" value="Upload New Picture" class="btn btn-default btn-upload-photo"> -->
				<input type="file" name="uploadPhoto" id="selectedFile" class="btn btn-default btn-upload-photo" />
				<input type="hidden" name="picture" value="<?= $getDataProfileUsers->picture ?>"/>
				<!-- <input type="button" value="Upload New Picture" onclick="document.getElementById('selectedFile').click();" class="btn btn-default btn-upload-photo" />-->
				<input type="button" name="" value="Delete" class="btn btn-default btn-delete-photo"> 
			</div>

			<div class="input-edit-profile-place">
				<div class="form-group">
					<label class="text-color-main">Name</label>
					<input type="text" required class="form-control" name="name" placeholder="Enter your username" value=<?= $getDataProfileUsers->name ?>>
					<img src="<?= base_url()?>assets/client/img/calendar-icon.svg" class="visibility-hidden" >
				</div>
				<div class="form-group">
					<label class="text-color-main">Email</label>
					<input type="email" required class="form-control" name="email" placeholder="Enter your email" value=<?= $getDataProfileUsers->email ?>>
					<img src="<?= base_url()?>assets/client/img/calendar-icon.svg" class="visibility-hidden" >
				</div>
				<!-- <div class="form-group">
					<label class="text-color-main">Password Old</label>
					<input type="password" class="form-control" placeholder="Enter your password" id="password-old">
					<span toggle="#password-old" class="fa fa-fw fa-eye new-password-icon toggle-new-password"></span>
					<img src="<?= base_url()?>assets/client/img/calendar-icon.svg" class="visibility-hidden" >
				</div>
				<div class="form-group">
					<label class="text-color-main">Password New</label>
					<input type="password" class="form-control" placeholder="Enter your password" id="password-new">
					<span toggle="#password-new" class="fa fa-fw fa-eye new-password-icon toggle-new-password"></span>
					<img src="<?= base_url()?>assets/client/img/calendar-icon.svg" class="visibility-hidden" >
				</div> -->
				<div class="form-group">
					<label class="text-color-main">Telephone</label>
					<input type="number" req class="form-control" name="phone" placeholder="Enter your telephone" value=<?= $getDataProfileUsers->phone ?>>
					<img src="<?= base_url()?>assets/img/client/calendar-icon.svg" class="visibility-hidden" >
				</div>
				<div class="form-group">
					<div class="pull-right">
						<input type="submit" name="submit" value="Save" class="btn btn-default btn-edit-profile">
					</div>
				</div>
			</div>
		</form>
	
	</div>
	<div class="info-profile">
		<img src="<?= base_url()?>assets/img/client/<?=$getDataProfileUsers->picture?>" class="info-photo-profile">
		<div class="text-username">
			<span class=" main-color"><?= $getDataProfileUsers->name?></span>	
		</div>
		<div class="text-email">
			<span class=" ongoing-color"><?= $getDataProfileUsers->email?></span>	
		</div>
		<div class="text-email">
			<span class=" ongoing-color"><?= $getDataProfileUsers->company?></span>	
		</div>
		<div class="text-phone">
			<span class=" ongoing-color"><?= $getDataProfileUsers->phone?></span>
		</div>
	</div>
	<?php endforeach ?>
</div>

<div class="spacer"></div>
<div class="project-name">
	Project 1
</div>
<br><br>
<div class="icons-text-container">
	<div>
		<div class="alifia">
			<div class="blank-left"></div>
			
			<a href="<?php echo base_url();?>index.php/client/MyProject/detailProjectRequest"> 
			<div class="circle-active"><img src="<?php echo base_url();?>assets/img/icons/icon-clock-active.png" style="margin: 25%; width:50%"></div>
			</a>
			
			<div class="line-active"></div>
			<div class="circle-active"><img src="<?php echo base_url();?>assets/img/icons/icon-person-active.png" style="margin: 30%; width:40%"></div>
			<div class="line"> </div>
			<div class="circle"><img src="<?php echo base_url();?>assets/img/icons/icon-sync.png" style="margin: 30%; width:40%"></div>
			<div class="line"> </div>
			<div class="circle"> <img src="<?php echo base_url();?>assets/img/icons/icon-book.png" style="margin: 30%; width:40%;"></div>
			<div class="line"> </div>
			<div class="circle"> <img src="<?php echo base_url();?>assets/img/icons/icon-check.png" style="margin: 30%; width:40%"></div>
			<div class="blank-left"></div>
		</div>
		<div class="alifia">
			<div class="text-alifi text-1">Choose avalaible inspector</div>
		</div>
	</div>
</div>
<br><br>

<div class="container-fluid">
	<div class="detail-project-place-choose-inspector">
			<span class="choose-title">View All Inspector</span>
			<hr>
			<p class="choose-note">Please , select your the appropriate inspector.</p>
		<div class="card-place">
			<div class="card-inspector">
				<div class="card-inspector-content">
					<div class="profile">
					<center>
						<div>
					 <span class="name">Mr.Farhan</span>
						</div>
						<div>
						<img src="<?=base_url()?>assets/img/client/profile-bg.png" class="photo-profile">
						</div>
						<div>
							View Detail
						</div>
						</center>
					</div>
					<div class="description">
						<div></div>
						<div>
						<span class="content">Mr. Farhan lorem ipsum Lorem ipsum dolor sit amet, 
						consectetur adipiscing elit, sed do eiusmod tempor . </span>
						</div>
						<div class="btn-place">
						<button class="btn-aggree">Agree</button>
						<button class="btn-disagree">Disagree</button>
						</div>
					</div>
				</div>
			</div>
			<div class="card-inspector">
				<div class="card-inspector-content">
					<div class="profile">
					<center>
						<div>
					 <span class="name">Mr.Farhan</span>
						</div>
						<div>
						<img src="<?=base_url()?>assets/img/client/profile-bg.png" class="photo-profile">
						</div>
						<div>
							View Detail
						</div>
						</center>
					</div>
					<div class="description">
						<div></div>
						<div>
						<span class="content">Mr. Farhan lorem ipsum Lorem ipsum dolor sit amet, 
						consectetur adipiscing elit, sed do eiusmod tempor . </span>
						</div>
						<div class="btn-place">
						<button class="btn-aggree">Agree</button>
						<button class="btn-disagree">Disagree</button>
						</div>
					</div>
				</div>
			</div>
			<div class="card-inspector">
				<div class="card-inspector-content">
					<div class="profile">
					<center>
						<div>
					 <span class="name">Mr.Farhan</span>
						</div>
						<div>
						<img src="<?=base_url()?>assets/img/client/profile-bg.png" class="photo-profile">
						</div>
						<div>
							View Detail
						</div>
						</center>
					</div>
					<div class="description">
						<div></div>
						<div>
						<span class="content">Mr. Farhan lorem ipsum Lorem ipsum dolor sit amet, 
						consectetur adipiscing elit, sed do eiusmod tempor . </span>
						</div>
						<div class="btn-place">
						<button class="btn-aggree">Agree</button>
						<button class="btn-disagree">Disagree</button>
						</div>
					</div>
				</div>
			</div>
			<div class="card-inspector">
				<div class="card-inspector-content">
					<div class="profile">
					<center>
						<div>
					 <span class="name">Mr.Farhan</span>
						</div>
						<div>
						<img src="<?=base_url()?>assets/img/client/profile-bg.png" class="photo-profile">
						</div>
						<div>
							View Detail
						</div>
						</center>
					</div>
					<div class="description">
						<div></div>
						<div>
						<span class="content">Mr. Farhan lorem ipsum Lorem ipsum dolor sit amet, 
						consectetur adipiscing elit, sed do eiusmod tempor . </span>
						</div>
						<div class="btn-place">
						<button class="btn-aggree">Agree</button>
						<button class="btn-disagree">Disagree</button>
						</div>
					</div>
				</div>
			</div>
			<!-- <div class="card-inspector">as</div>
			<div class="card-inspector">as</div>
			<div class="card-inspector">as</div>	 -->
		</div>
	</div>
</div>
<div class="spacer"></div>
